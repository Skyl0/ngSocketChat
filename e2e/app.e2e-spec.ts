import { NgSocketChatPage } from './app.po';

describe('ng-socket-chat App', () => {
  let page: NgSocketChatPage;

  beforeEach(() => {
    page = new NgSocketChatPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
