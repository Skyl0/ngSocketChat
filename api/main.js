// main.js
const userRoutes = require('./_user');

module.exports = function (app, db, io, jwt, cpriv) {

  userRoutes(app, db, io, jwt, cpriv);
  // add more entrypoints
}
