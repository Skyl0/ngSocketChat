// _user.js

const SHA256 = require("crypto-js/sha256");

module.exports = function (app, db, socket, jwt, sslPrivate) {

  const entrypoint = "/api";
  const User = db.collection("user");

  User.createIndex({"username": 1}, {unique: true});
  User.createIndex({"alias": 1}, {unique: true});

  /**
   * POST
   * REGISTER
   */
  app.post(entrypoint + '/register', function (req, res) {
    // register
    if (req._body) {
      // Create "hidden" fields
      var model = req.body;
      // hash the password

      model.password = SHA256(req.body.password).toString();

      User.insertOne(model, function (err, result) {
        if (!err) {
          res.send(result);
        } else {
          error = {'message': 'Username or Alias taken, try again.'};
          res.send(error);
        }
      });
      // res.send("Error Username already taken!");
    }
  });

  /**
   * POST
   * LOGIN
   */
  app.post(entrypoint + '/login', function (req, res) {
    // login
    User.findOne({username: req.body.username}, {username: 1, alias: 1, password: 1}, function (err, user) {
      if (err) throw err;

      if (!user) {
        res.json({success: false, message: 'Authentication failed. User not found.'});
      } else if (user) {

        // check if password matches
        if (user.password != SHA256(req.body.password).toString()) {
          res.json({success: false, message: 'Authentication failed. Wrong password.'});
        } else {
          var token = jwt.sign({identifier: user.alias}, sslPrivate, { algorithm: 'RS256'});

          // return the information including token as JSON
          res.json({
            success: true,
            token: token
          });
        }
      }
    })

  });

  app.get(entrypoint + "/user", function (req, res) {
    // get userdata
    var alias = req.decoded.identifier;
    console.log("GETUSER" + alias);
    User.findOne({alias: alias}, {alias: 1, username: 1}, function (err, user) {
      if (err) throw err;
      console.log(user);
      res.send(user);
    })
  });

  app.get(entrypoint + "/user/:alias", function (req, res) {
    // get userdata
    //var alias = req.decoded.identifier;
    User.findOne({alias: req.params.alias}, {alias: 1}, function (err, user) {
      if (err) throw err;
      result = user;
      res.send(result);
    })
  });

};
