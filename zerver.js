/***************************
 * Title : ngSocketChat 0.1
 * - - - - - - - - - - - - -
 * Owner : Marc Ernst
 * E-Mail : ernst@skylo.de
 * License Type : MIT
 * Date : 06.07.17
 * Filename : run_node_server.js
 * Class :
 ***************************/
const express = require('express');
const path = require('path');
const app = express();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const io = require('socket.io')(server);
const MongoClient = require('mongodb').MongoClient;
const fs = require('fs');
const jwt = require('jsonwebtoken');

const cpriv = fs.readFileSync("./cert/priv_node.pem");
const cpub = fs.readFileSync("./cert/pub_node.pem");

app.use(express.static(path.join(__dirname, "/dist")));
app.use(express.static(path.join(__dirname, "/public")));

const url_mongo = 'mongodb://database/chatCity'; // TODO CONFIG

/** Load Bodyparser **/

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

MongoClient.connect(url_mongo, function (err, db) {
  /**
   * IF MONGO CONNECTED
   *
   * */
  if (!err) {
    console.log("MongoDB Connect successful");
  }

  /** Routes **/
  var apiRoutes = express.Router();
  apiRoutes.use(function (req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {

      // verifies secret and checks exp
      jwt.verify(token, cpub, function (err, decoded) {
        if (err) {
          return res.json({success: false, message: 'Failed to authenticate token.'});
        } else {

          // if everything is good, save to request for use in other routes
          req.decoded = decoded;
          //  console.log(decoded);
          next();
        }
      });

    } else {

      // if there is no token
      // return an error
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });

    }
  });

  app.use('/api/user', apiRoutes);
  app.use('/api/userlist', apiRoutes);
  app.use('/api/user/*', apiRoutes);

  require('./api/main')(app, db, io, jwt, cpriv);

  /**
   * ENDIF MONGO CONNECTED
   *
   * */

  users = {};

  io.on('connection', function (socket) {

    socket.on('chat message', function (msg) {
      io.emit('chat message', msg);
    });

    socket.on('adduser', function (alias) {
      socket.username = alias;
      users[alias] = alias;
      io.emit('chat message', {sender: 'SYSTEM', msg: 'User joined', timestamp: Date.now()});
    });

    socket.on('disconnect', function () {
      delete users[socket.username];
      io.emit('chat message', {sender: 'SYSTEM', msg: 'User left', timestamp: Date.now()});
    });

  });

  app.get('/api/userlist', function (req, res) {
    var userList = Object.keys(users);
    res.send(userList);
  });

  app.get('**', function (req, res) {
    res.sendFile(__dirname + '/dist/index.html');
  });

  server.listen(8000, function () {
    console.log('Listening on port 8000!');
  });
});
