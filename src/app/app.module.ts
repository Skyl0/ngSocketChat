import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserService} from './_services/user.service';
import {AppComponent} from './app.component';
import {LandingComponent} from './_components/landing/landing.component';
import {ChatComponent} from './_components/chat/chat.component';
import {AuthComponent} from './_components/auth/auth.component';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {ChatService} from './_services/chat.service';
import {UnixTsPipe} from './_helper/unix-ts.pipe';


const appRoutes: Routes = [
  {path: 'chat', component: ChatComponent },
  {path: 'register', component: AuthComponent},
  {path: '**', component: LandingComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    ChatComponent,
    AuthComponent,
    UnixTsPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [
    UserService,
    ChatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
