import {Component, OnInit} from '@angular/core';
import {UserService} from '../../_services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styles: []
})
export class AuthComponent implements OnInit {

  regModel: any = {};
  loginModel: any = {};
  controlPwd = '';

  constructor(private usrServ: UserService, private router: Router) {
  }

  ngOnInit() {
  }

  registerUser() {
    if (this.regModel.password === this.controlPwd) {
      this.router.navigate(['/']).then(() => {
        this.usrServ.registerUser(this.regModel);
      });
    } else {
      console.log('not registered, passwords don`t match');
    }

  }

  loginUser() {
    this.usrServ.loginUser(this.loginModel);
  }

}
