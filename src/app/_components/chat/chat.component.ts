import {Component, OnDestroy, OnInit} from '@angular/core';
import {Message} from '../../_model/message';
import {ChatService} from '../../_services/chat.service';
import {UserService} from '../../_services/user.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styles: []
})
export class ChatComponent implements OnInit, OnDestroy {


  messages: Message[] = [];
  messageToSend = '';
  connection: any;

  constructor(public chatServ: ChatService, public userServ: UserService) {

  }

  subscribeChat() {
    this.connection = this.chatServ.getMessages().subscribe(message => {
      console.log(message);
      this.messages.push(message);
    });
  }

  sendMsg() {
    const newMsg = new Message(this.userServ.currentAlias, this.messageToSend, Date.now());
    this.messageToSend = '';
    this.chatServ.sendMessage(newMsg);
  }

  ngOnInit() {
    this.subscribeChat();
   // $('.nano').nanoScroller();
  }

  ngOnDestroy(): void {
    this.chatServ.leave();
  }

  handleEvent($event) {
    if ($event.which === 13 || $event.keyCode === 13) {
      this.sendMsg();
    }
  }

}
