import {Injectable} from '@angular/core';
import {User} from '../_model/user';
import {JwtHelper, tokenNotExpired} from 'angular2-jwt';
import 'rxjs/add/operator/map';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import {Router} from '@angular/router';


@Injectable()
export class UserService {

  public token: string;
  public currentUser: User;
  public currentAlias = '';
  jwtHelper: JwtHelper = new JwtHelper();

  private emptyUser: User = {
    username: '',
    alias: ''
  };

  constructor(private http: Http, private router: Router) {
    this.currentUser = this.emptyUser;
    this.decodeAlias();
  }

  loggedIn() {
    return tokenNotExpired();
  }

  logout() {
    localStorage.removeItem('token');
    this.currentUser = this.emptyUser;
    this.currentAlias = '';
  }

  decodeAlias() {

    if (this.loggedIn()) {
      this.token = localStorage.getItem('token');
      const decoded = this.jwtHelper.decodeToken(this.token);
      console.log(decoded);
      this.currentAlias = decoded.identifier;
    }
  }

  loginUser(credentials: any): any {
    this.login(credentials)
      .subscribe(
        data => {
          console.log(data);
          const token = data.token;
          if (token) {
            // set token property
            this.token = token;

            // store username and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('token', this.token);
            this.decodeAlias();

            this.router.navigate(['/']).then(() => {
                this.getUser().subscribe(
                  _data => {
                    this.currentUser = data;
                    console.log(this.currentUser);
                    return true;
                  },
                  error => {
                    return false;
                  }
                );
              }
            );


            // return true to indicate successful login
          } else {
            // return false to indicate failed login
            return false;
          }
        }
      )
    // login successful if there's a jwt token in the response

  }

  private login(credentials: any) {

    const headers = new Headers({'x-access-token': localStorage.getItem('token')});
    const options = new RequestOptions({headers: headers});

    return this.http.post('/api/login', credentials, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }


  registerUser(newUser: User) {

    this.register(newUser)
      .subscribe(
        data => console.log('successful registered.'),
        err => console.log(err),
        () => console.log('post complete.'));
  }

  private register(newUser: User) {

    return this.http.post('/api/register', newUser, {})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  private getUser(): any {
    // get users from api

    const headers = new Headers({'x-access-token': localStorage.getItem('token')});
    const options = new RequestOptions({headers: headers});


    return this.http.get('/api/user', options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  getChatUserlist() {
    const headers = new Headers({'x-access-token': localStorage.getItem('token')});
    const options = new RequestOptions({headers: headers});

    return this.http.get('/api/userlist', options).map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getCurrent(): any {
    return new Promise((resolve, reject) => {
      if (this.currentUser.alias !== '') {
        resolve(this.currentUser);
      } else {
        this.getUser().subscribe(
          data => {
            //   console.log(data);
            this.currentUser = data;
            resolve(this.currentUser);
          },
          error => {
            console.log(error);
            reject();
          }
        );
      }
    });

  }

}
