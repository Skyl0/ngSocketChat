import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import 'rxjs/Rx';
import {Message} from '../_model/message';
import {Observable} from 'rxjs/Observable';
import {UserService} from './user.service';
import {Http} from '@angular/http';

@Injectable()
export class ChatService {

  private socket;
  public userlist;

  constructor(private userServ: UserService, private http: Http) {
    this.socket = io();
    this.socket.emit('adduser', this.userServ.currentAlias);
  }

  sendServiceMsg(sysmsg: string, obj: any) {
    this.socket.emit(sysmsg, obj);
  }

  sendMessage(message: Message) {

    this.socket.emit('chat message', message);
  }

  getMessages(): Observable<Message> | any {

    const observable = new Observable<Message>(observer => {
      this.socket.on('chat message', (data) => {

        if (data.sender === 'SYSTEM' && (data.msg === 'User joined' || data.msg === 'User left')) {
          this.userServ.getChatUserlist().subscribe(
            _data => {
              this.userlist = _data;
              console.log(this.userlist);
            },
            error => {
              console.log(error);
            });
        }

        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  leave() {
    this.socket.emit('disconnect');
  }


}
