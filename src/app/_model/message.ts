export class Message {
  sender: string;
  msg: string;
  timestamp: any;

  constructor(sender: string, msg: string, timestamp: any) {
    this.sender = sender;
    this.msg = msg;
    this.timestamp = timestamp;
  }
}
